package graph

// IsAcyclic determines if the graph has cycles or not
func (g *Graph) IsAcyclic() bool {
	g.lock.Lock()
	defer g.lock.Unlock()
	vmap := g.Verticies()
	for {
		if len(vmap) == 0 {
			break
		}
		var start Vertex
		for _, v := range vmap {
			start = v
			break
		}

		hasC, visited := hasCycle(start, map[int]Vertex{})
		if hasC {
			return false
		}
		for key := range visited {
			delete(vmap, key)
		}
	}
	return true
}

func hasCycle(v Vertex, visited map[int]Vertex) (bool, map[int]Vertex) {
	_, ok := visited[v.ID()]
	visited[v.ID()] = v
	if ok {
		return true, visited
	}

	for nx := range v.To() {
		isC, visited := hasCycle(nx, visited)
		if isC {
			return true, visited
		}
	}
	return false, visited
}

// Independents returns a list of all independent verticies that have no incoming edges
func (g *Graph) Independents() []Vertex {
	indep := []Vertex{}
	for vID1, v1 := range g.vertices {
		isIndependent := true
		for _, v2 := range g.vertices {
			for v3 := range v2.To() {
				if v3.ID() == vID1 {
					isIndependent = false
				}
			}
		}
		if isIndependent {
			indep = append(indep, v1)
		}
	}
	return indep
}

// CriticalPath returns the critical path through a directed acyclic graph (DAG)
func (g *Graph) CriticalPath() ([]Vertex, float64, error) {
	isa := g.IsAcyclic()
	if !isa {
		return []Vertex{}, 0, StructureError{}
	}
	iv := g.Independents()
	switch len(iv) {
	case 0:
		return []Vertex{}, 0, StructureError{}
	case 1:
		cp, cpw := g.cp(iv[0], 0, []Vertex{}, 0, []Vertex{})
		return cp, cpw, nil
	default:
		// there are > 1 independent tasks
		path := []Vertex{}
		weight := 0.0
		for _, v := range iv {
			tmpPath, tmpW := g.cp(v, 0, []Vertex{}, 0, []Vertex{})
			if tmpW > weight {
				path = tmpPath
				weight = tmpW
			}
		}
		return path, weight, nil
	}
}

func (g *Graph) cp(v Vertex, cpw float64, cp []Vertex, mpw float64, mp []Vertex) ([]Vertex, float64) {
	for nextV, weight := range v.To() {
		tmpMPW := mpw + weight
		tmpMP := append(mp, nextV)

		if tmpMPW > cpw {
			cpw = tmpMPW
			cp = tmpMP
		}
		tmpCP, tmpCPW := g.cp(nextV, cpw, cp, tmpMPW, tmpMP)
		if tmpCPW > cpw {
			cpw = tmpCPW
			cp = tmpCP
		}
	}
	return cp, cpw
}
