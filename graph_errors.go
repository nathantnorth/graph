package graph

import "fmt"

// VertexNotFoundError describes an error where a vertex does not exist or cannot be found
type VertexNotFoundError struct {
	ID     int
	Detail string
}

func (e VertexNotFoundError) Error() string {
	baseError := fmt.Sprintf("task id %v not found", e.ID)
	if len(e.Detail) > 0 {
		baseError += " : " + e.Detail
	}
	return baseError
}

// StructureError defines an error with the structure of the graph
type StructureError struct{}

func (e StructureError) Error() string {
	return "structure of graph does not meet requirements for given function call"
}
