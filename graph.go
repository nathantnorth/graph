package graph

import (
	"fmt"
	"sync"
)

// Vertex creates a node in the graph structure
type Vertex interface {
	ID() int
	ConnectTo(v Vertex, weight ...float64)
	To() map[Vertex]float64
}

// Graph is a generic graph data structure
type Graph struct {
	lock     sync.Mutex
	vertices map[int]Vertex
}

// New creates an empty graph and initializes the map
func New() *Graph {
	g := Graph{}
	g.vertices = make(map[int]Vertex)
	return &g
}

// AddVertex adds the given vertex to the graph
func (g *Graph) AddVertex(v Vertex) error {
	g.lock.Lock()
	defer g.lock.Unlock()
	_, ok := g.vertices[v.ID()]
	if ok {
		return fmt.Errorf("vertex with given ID already exists")
	}
	g.vertices[v.ID()] = v
	return nil
}

// GetVertex returns the vertex with the given ID
func (g *Graph) GetVertex(id int) (Vertex, bool) {
	v, ok := g.vertices[id]
	return v, ok
}

// AddArc adds an edge to the graph and returns an error if the given vertex IDs can't be found
func (g *Graph) AddArc(from, to int, weight ...float64) error {
	g.lock.Lock()
	defer g.lock.Unlock()
	fromV, ok := g.GetVertex(from)
	if !ok {
		return VertexNotFoundError{ID: from}
	}
	toV, ok := g.GetVertex(to)
	if !ok {
		return VertexNotFoundError{ID: to}
	}
	fromV.ConnectTo(toV, weight...)
	return nil
}

// HasArc determines if there is an edge between the two given verticies
func (g *Graph) HasArc(from, to int) bool {
	fromV, ok := g.GetVertex(from)
	if !ok {
		return false
	}
	toMap := fromV.To()
	for v := range toMap {
		if v.ID() == to {
			return true
		}
	}
	return false
}

// Verticies returns a mapping of all verticies in the graph
func (g *Graph) Verticies() map[int]Vertex {
	return g.vertices
}

// Size returns the total number of verticies in the graph
func (g *Graph) Size() int {
	return len(g.vertices)
}
