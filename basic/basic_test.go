package basic

import (
	"gitlab.com/nathantnorth/graph"
)

func ExampleGraph_AddVertex() {
	g := graph.New()
	for i := 0; i < 4; i++ {
		g.AddVertex(&Node{id: i})
	}
	_ = g.AddArc(0, 1)
}
