package basic

import (
	"gitlab.com/nathantnorth/graph"
)

// Node is a basic implementation of a graph node and implements the Vertex interface
type Node struct {
	id    int
	edges map[graph.Vertex]float64
}

// ID returns the node's ID
func (n *Node) ID() int {
	return n.id
}

// ConnectTo adds an edge between the node and the input node
// If that edge already exists, it will update the weight to be either 0
// or the given weight
func (n *Node) ConnectTo(v graph.Vertex, weight ...float64) {
	var edgeWeight float64
	if len(weight) > 0 {
		edgeWeight = weight[0]
	}
	if len(n.edges) == 0 {
		n.edges = map[graph.Vertex]float64{
			v: edgeWeight,
		}
	} else {
		n.edges[v] = edgeWeight
	}
}

// To returns the map of graph edges and their weights
func (n *Node) To() map[graph.Vertex]float64 {
	return n.edges
}
